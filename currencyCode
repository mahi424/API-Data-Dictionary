License:            This Data Element licensing falls within the Creative Commons Attribution 4.0 International Public 
                    License described in 0.1 License

Element Name:       currencyCode         
Definition:         A currency code is a three-letter suffix such as USD (United States Dollar), GBP (Great Britain Pound) and EUR (Euro) assigned to a currency. 
                    For a list of codes, see ISO-4217 and/or UN/ECE REC 09 Codes
Description:        currency code is a 3 uppercase alphabetic character enumeration of a currency as defined in ISO-4217 / UN/ECE REC 09 Codes
Alias:              CurrencyCode, Currency

Type:               String 
Content:            String - exactly 3-alphabetic uppercase characters; white space, numbers or specical characters are not allowed
                    The content is always one of the enumerations - nothing else is allowed.
           
Where used:         This element is used in the following objects and APIs
Objects:            objectName1
APIs:               [Collection Name] API Action /API name
                    e.g. 
                    [IFSF Wet Stock Management Agent] GET /sites/{siteID}
                    [IFSF Wet Stock Management Agent] GET /sites/{siteID}/reconciliationReports
                    etc.,
    
YAML code:          - The text between brackets can be cut and paste into the Online Swagger editor (include a hyper link)
[
currencyCodeEnum:
      description: ISO-4217 / UN/ECE REC 09 Codes
      enum:
        - ADP
        - AED
        - AFA
        - ALL
        - AMD
        - ANG
        - AOA
        - ARS
        - ATS
        - AUD
        - AWG
        - AZM
        - BAM
        - BBD
        - BDT
        - BEF
        - BGL
        - BGN
        - BHD
        - BIF
        - BMD
        - BND
        - BOB
        - BOV
        - BRL
        - BSD
        - BTN
        - BWP
        - BYR
        - BZD
        - CAD
        - CDF
        - CHF
        - CLF
        - CLP
        - CNY
        - COP
        - CRC
        - CUP
        - CVE
        - CYP
        - CZK
        - DEM
        - DJF
        - DKK
        - DOP
        - DZD
        - ECS
        - ECV
        - EEK
        - EGP
        - ERN
        - ESP
        - ETB
        - EUR
        - FIM
        - FJD
        - FKP
        - FRF
        - GBP
        - GEL
        - GHC
        - GIP
        - GMD
        - GNF
        - GRD
        - GTQ
        - GWP
        - GYD
        - HKD
        - HNL
        - HRK
        - HTG
        - HUF
        - IDR
        - IEP
        - ILS
        - INR
        - IQD
        - IRR
        - ISK
        - ITL
        - JMD
        - JOD
        - JPY
        - KES
        - KGS
        - KHR
        - KMF
        - KPW
        - KRW
        - KWD
        - KYD
        - KZT
        - LAK
        - LBP
        - LKR
        - LRD
        - LSL
        - LTL
        - LUF
        - LVL
        - LYD
        - MAD
        - MDL
        - MGF
        - MKD
        - MMK
        - MNT
        - MOP
        - MRO
        - MTL
        - MUR
        - MVR
        - MWK
        - MXN
        - MXV
        - MYR
        - MZM
        - NAD
        - NGN
        - NIO
        - NLG
        - NOK
        - NPR
        - NZD
        - OMR
        - PAB
        - PEN
        - PGK
        - PHP
        - PKR
        - PLN
        - PTE
        - PYG
        - QAR
        - ROL
        - RUB
        - RUR
        - RWF
        - SAR
        - SBD
        - SCR
        - SDD
        - SEK
        - SGD
        - SHP
        - SIT
        - SKK
        - SLL
        - SOS
        - SRG
        - STD
        - SVC
        - SYP
        - SZL
        - THB
        - TJS
        - TMM
        - TND
        - TOP
        - TPE
        - TRL
        - TTD
        - TWD
        - TZS
        - UAH
        - UGX
        - USD
        - USN
        - USS
        - UYU
        - UZS
        - VEB
        - VND
        - VUV
        - WST
        - XAF
        - XAG
        - XAU
        - XBA
        - XBB
        - XBC
        - XCD
        - XDR
        - XFO
        - XFU
        - XOF
        - XPD
        - XPF
        - XPT
        - XTS
        - YER
        - YUM
        - ZAR
        - ZMK
        - ZWD
      type: string
    currencyCode:
      anyOf:
        - $ref: '#currencyCodeEnum'
        - type: string
]